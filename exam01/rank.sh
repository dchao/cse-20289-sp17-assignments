usage(){
	cat<<EOF
	Usage: rank.sh [ -n N -D]
		-n N		Returns N items (default is 5)
		-D 			rank in descending order

EOF
exit $1
}


cat output <<EOF

EOF

returnItems=5				#default value
descend=0					#default value used as a bool

for VAR in $@
do
	case $1 in
		"-h")
			usage
		;;
		"-n")
			shift
			$returnItems=$1
		;;
		"-D")
			$descend=1
		;;
		*)
			while read -r LINE
			do
				echo "$LINE" >> output
			done
		;;
	esac
done

if [ $descend -eq 0 ]; then			#ascending order
	cat output | sort -n | head -n $returnItems
else
	cat output | sort -nr | head -n $returnItems
fi
