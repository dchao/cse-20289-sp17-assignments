GRADING - Exam 01
=================

- Commands:         2.25
- Short Answers:
    - Files:        2.25 
    - Processes:    2.5 
    - Pipelines:    2.5 
- Debugging:        2.75
- Code Evaluation:  2.25 
- Filters:          2.75
- Scripting:        2.5 
- Total:	    19.75
