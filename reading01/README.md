Reading 01
==========
1) a) the purpose of the | is to pipe the standard output of du and 
   makes it the standard input of sort
   b) the purpose of the 2> /dev/null is to direct the standard error to
   /dev/null to dispose of the error
   c) the purpose of > output.txt is to direct the output of sort to 
   output.txt
   d) the purpose of the -h flag is to make the file size in human 
   readable form rather than bytes
   e) no. the standard error of the sort will be sent to /dev/null and 
   would be disposed of as opposed to the standard error of du
   
2) a) cat [2]200 > 2002.txt
   b) cat *12 > December.txt
   c) cat *{01..06}
   d) cat *{2002,2004,2006}{01,03,05,07,09,11}
   e) cat *{2002..2004}{09..12}

3) a) Huxley and Tux
   b) Huxley
   c) None
   d) o+x, o+r
   e) a-x, g-r
   
4) a) kill -STOP
   b) kill -CONT
   c) ctrl-d
   d) kill -TERM
   e) use: ps to find the PID of the process
   	  then use: kill $(PID)
