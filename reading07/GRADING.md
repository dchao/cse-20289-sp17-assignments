Reading 07 - Grading
========================
**Score**: 2.25/4

**Grader**: Mimi Chen


Deductions
------------------
(-0.5) Makefile does not work

(-0.25) Failed test_sort.sh

(-0.25) sort_numbers was not implemented correctly. You need to swap numbers[i] with numbers[i-1]

	void sort_numbers(int numbers[], size_t n) {
		
		for (size_t i = 1; i < n; i++) {
			int j = i;
			while (j > 0 && numbers[j] < numbers[j-1])
			{
				int temp = numbers[j];
				numbers[j] = numbers[j - 1];
				numbers[j - 1] = temp;
				j = j-1;
			}
		}
	}

(-0.25) Result of sort is not printed correctly

(-0.25) Failed test_grep.sh

(-0.25) grep_stream function should take a pattern and not use the global REGEX


Comments
------------------
