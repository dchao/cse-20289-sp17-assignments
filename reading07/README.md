Reading 07
==========
Activity 1
1) the ampersand in front of numbers[i] tells scanf that the variable we are looking for with the "%d" is numbers[i]; it is very similar to a pointer.

2) no, in C arrays decay to pointers to their first element, so in this case size of would return the size of an integer and not of the array.

Activity 2
1) Both cat.c and cat.py parse the command line arguments as a vector of strings that are manipulated in some way later in order to determine if flags are being passed to the script.  However, cat.c iterates through this vector while cat.py passes over each object by performing some task and then popping this last argument from the vector.  In cat.c argc is used to iterate over each arguemtn passed as a type of counter, so that while there are still arguments that have not been checked (argind < argc) we still check what other arguments we must parse

2) the while loop does a string comparison on the path to determine whether or not the items it must output are from a pipeline (the standard in within if statemet) or if it is a file that must be opened (the path within the else statement).

3) cat_stream works by using fgets to read each line of the stream into a string that is then passed to fputs and subsequently to the standard output stream.  This stream (stdout) is later sent out

4) cat_file uses an if statement to check if the file is NULL, or rather if the file exists or not.  If the file does not exist, it prints the error, the program name, the file and the error associated with the error number caused by the null file (passing errno (the error number of the last error) to strerror (the explanation associated with the given error)).  If the file is valid, it is then passed to cat_stream. 
