Reading 08
==========
Activity 1
1) int i = 0
   4 bytes
2) int a[] = {4,6,6,3,7}
   5 ints x 4 bytes/int = 20 bytes
3) char *s = "pgui"
   1 byte/char x 4 chars + 1 byte (NULL char) = 5 bytes
4) struct point p = {0,0}
   2 x 8 bytes = 16 bytes 
5) struct point *q = NULL
   2 x 8 bytes = 16 bytes
6) struct point *x = malloc(sizeof(struct point))
   16 bytes (#4)
7) struct point *y = malloc(10*sizeof(struct point))
   16 bytes (#6) x 10 = 160 bytes
8) struct point **z = malloc(10*sizeof(struct point *))
   80 bytes*

Activity 2
1) The error was "Inavlid write size of 4" in line 20 (int *randoms=malloc(n) (n is total number of ints)), meaning I was tring to access memory which had yet to be allocated since malloc requires the number of bytes to allocate.  In this case, since sizeof was not used, we we're only allocating 1 byte per int instead of the proper 4 bytes per int, so n*sizeof(int) instead of just n to properly allocate the memory and avoid this error.  
2) The error was "409,600 bytes in 100 blocks are definitely lost in loss record 1 of 1", meaning the memory leak that I had was that I would only free the memory from the heap if the statement was false, not when it was also true, so simply adding the free(randoms) command before returning true fixed this issue. 
