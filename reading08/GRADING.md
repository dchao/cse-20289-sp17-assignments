Reading 08
==========

**Score**: 2.75/4

Deductions
----------
* `-.25` README #3 - 8 + 5 * 1 = 13 bytes. Pointer is 8 bytes + 5 chars (need to include NUL)
* `-.25` README #5 - a pointer is just 8 bytes
* `-.25` README #6 - 8 + 16 = 24 bytes. Pointer is 8 bytes, and one point struct is 16 bytes
* `-.25` README #7 - 8 + 10 * 16 = 168 bytes Pointer is 8 bytes, and 10 point structs
* `-.25` README #8 8 + 10 * 8  = 88 bytes. Pointer is 8 bytes, and 10 pointers to structs

Comments
--------
