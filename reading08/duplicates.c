/* duplicates.c */

#include <stdbool.h>
#include <stdio.h>
#include <stdlib.h>
#include <time.h>

const int NITEMS = 1<<10;
const int TRIALS = 100;

bool contains(int *a, int n, int key) 
{
    /* iterate through array to search for key */
    for (int i = 0; i < n; i++)
    {
        if (a[i] == key)
            return true;
    }

    return false;
}

bool duplicates(int n) 
{
    int *randoms = malloc(n *(sizeof(int)));

    for (int i = 0; i < n; i++)
    {
        randoms[i] = rand() % 1000;
    }

    for (int i = 0; i < n; i++) 
    {
        /* Contains will search for the duplicates */
        if (contains(randoms, n, randoms[i+1])) 
        {
            free(randoms);
            return true;
        }
    }

    free(randoms);
    return false;
}

int main(int argc, char *argv[]) {
    srand(time(NULL));

    for (int i = 0; i < TRIALS; i++)
        if (duplicates(NITEMS))
            puts("Duplicates detected!");

    return 0;
}

/* vim: set sts=4 sw=4 ts=8 expandtab ft=c: */
