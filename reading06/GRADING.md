## GRADING
### Reading06
##### Deductions
-0.25:  evens_gr.py -- Use 'for number in stream' instead of 'stream.readline()'; See solution.
---
##### Final Grade: 3.75/4
---
*Graded by Maggie Thomann*
