#!/usr/bin/env python2.7

import sys

def evens(stream):
    for line in stream.readline():
	if (type(line) == 'string'):
	    line.strip()
	    int(line)
	    if (line % 2 == 0):
                yield line

print ' '.join(evens(sys.stdin))
