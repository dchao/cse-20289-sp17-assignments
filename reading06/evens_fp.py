#!/usr/bin/env python2.7

import sys

print ' '.join(map(lambda x: x.strip(), filter(lambda x: not int(x) % 2, sys.stdin)))
