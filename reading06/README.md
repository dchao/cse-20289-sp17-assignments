Reading 06
==========

1) MapReduce utilizes map to significantly decrease the execution time 
of a process: it splits up the task in parallel onto clusters made up of 
nodes (computers) that perform the task on smaller data sizes based on 
map.

2) "Map" step: each computer applies map to their local data 
   "Shuffle" step: the data is redistributed based on a parameter 
   specific to the working computer such that all data that fits this 
   parameter or key is local to the specified working computer
   "Reduce" step: computers process the data in parallel
