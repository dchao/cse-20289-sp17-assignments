Homework 07
===========

Activity 1:

1) In node_create, memory was allocated for the struct "node". Memory was requested for a char * (cstring), an int and a struct node *. To find the total number of bytes sizeof() was called. The memory that was allocated was used using malloc.
In node_delete, first the string was freed. Then it handles the recursive flag, and if it's true a while loop is called that iterates over the next node  if the value of the node is not NULL, and subsequently frees the string of next node and the node itself. A temporary node is used to hold the next node during deletion. If there is no need to recursivley call, the passed node is freed


2) Malloc is used to allocate for the list, which contains a head node, a tail node and a size (integer). The size of each node is included within the sizeof() call. For list_delete() the node_delete() function is called recursively. This will delete all the nodes in the list and each of its contents. Finally, the actual list is freed in the end.


3) The function converts the linked list to an array and then subsequently calls the internal sort function on this array. The array is initialized to begin with the head of the list and end with the tail. A for loop iterates over the list and sets the value of the node in the list to the value of the corresponding index in the array.  Average: qsort is O( nlog(n) ).  Worst case: qsort is O( n^2 ).  Space complexity: O( log(n) ).

4) Two functions are needed: reverse and list_reverse.  Reverse returns a swapped node, whcih is used recursively. list_reverse is simply a more convenient method for the user to call.  Reverse updates the head and tail of the passed list. Simply put, the function essentially swaps each node recursively until the list is flipped.  Average: O( nlog(n) ).  worst case: O( nlog(n) ).  space complexity: O( log(n) ).

5) The function operates under the notion of "Divide and conquer".
Split divides the passed list into a left and right sublist, each of approx. half the length of the initially passed list until the sublists are down to either 0 or 1 node long.  Merge is then called recursively on these sublists. Merge will merge these sublists in order. finally, merge will return a pointer to the head of the new sorted list. The tail is then set in the new list after the merging is complete.
Maximum levels for merge sort is: O( log(n) ). In theory, this can be expressed using a tree. The tree's height is O( log(n) ). For each subsequent level, O( log(n) ) work is done in the tree. Therefore merge sort: O (nlog(n)) for average, best and worst time complexities.
Space complexity is constant at each level of the tree. TThis is due to simply rearranging the list; the number of pointers will not increase. Thus the space complexity is: O( log(n) ).

Activity 2:

1) Quicksort worked faster but used more memory, which was surprising to me. This is most likely due to the fact that the quicksort function was built to expect an array, thus the data was duplicated. An array will always be continuous though, which is not the same for nodes which can be scattered across the heap. This is ultimately what made qsort faster than merge sort.  

2) In terms of complexity, quicksort has it the worst. In terms of real world performance, this heavily relies on the spacial locality of the data. Complexities dont paint the full picture: hardware should be considered, too. Complexity only measures growth, not absolute performance, which also depends on hardware.
