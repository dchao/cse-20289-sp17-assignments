#!/usr/bin/env python2.7

import sys
import os

#GLOBALS
size = 1
command = ''
switch = 1

while size <= 10000000:
    if switch % 2 == 0:
	command = 'shuf -i1-{0} -n {0} | ./measure ./lsort -n -q > /dev/null'.format(size)
	switch = switch + 1
    else:
	command = 'shuf -i1-{0} -n {0} | ./measure ./lsort -n > /dev/null'.format(size)
    os.system(command)
    if switch % 2 == 0:
	size = size * 10


