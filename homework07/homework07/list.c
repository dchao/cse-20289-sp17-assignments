/* list.c */

#include "list.h"

/* Internal Function Prototypes */

static struct node*	reverse(struct node *curr, struct node *next);
static struct node *	msort(struct node *head, node_compare f);
static void		split(struct node *head, struct node **left, struct node **right);
static struct node *	merge(struct node *left, struct node *right, node_compare f);

/**
 * Create list.
 *
 * This allocates a list that the user must later deallocate.
 * @return  Allocated list structure.
 */
struct list *	list_create() {
    struct list * newList = malloc(sizeof(struct list));
    
    if (newList == NULL)
    {
        printf("Unable to allocate new list");
        return NULL;
    }	
    
    newList->head = NULL;
    newList->tail = NULL;
    newList->size = 0;

    return newList;   
}

/**
 * Delete list.
 *
 * This deallocates the given list along with the nodes inside the list.
 * @param   l	    List to deallocate.
 * @return  NULL pointer.
 */
struct list *	list_delete(struct list *l) {
    l->head = node_delete(l->head, true);
    free(l);
    return NULL;
}

/**
 * Push front.
 *
 * This adds a new node containing the given string to the front of the list.
 * @param   l	    List structure.
 * @param   s	    String.
 */
void		list_push_front(struct list *l, char *s) {
    struct node * newNode = node_create(s, l->head);
    l->head = newNode;
    //l->size += 1;
    if (l->size == 0)
    {
        l->tail = newNode;
    }
    l->size = l->size + 1;
}

/**
 * Push back.
 *
 * This adds a new node containing the given string to the back of the list.
 * @param   l	    List structure.
 * @param   s	    String.
 */
void		list_push_back(struct list *l, char *s) {
    struct node * newNode = node_create(s, NULL);
    if (l->size == 0)
    {
        l->head = newNode;
        l->tail = newNode;
        l->size = 1;
    }
    else
    {
        l->tail->next = newNode;
        l->tail = newNode;
        l->size += 1;
    }
}

/**
 * Dump list to stream.
 *
 * This dumps out all the nodes in the list to the given stream.
 * @param   l	    List structure.
 * @param   stream  File stream.
 */
void		list_dump(struct list *l, FILE *stream) {
    struct node * traverse = l->head;
    while (traverse->next != NULL)
    {
        node_dump(traverse,stream);
        traverse = traverse->next;       
    }
    node_dump(traverse,stream);
}

/**
 * Convert list to array.
 *
 * This copies the pointers to nodes in the list to a newly allocate array that
 * the user must later deallocate.
 * @param   l	    List structure.
 * @return  Allocate array of pointers to nodes.
 */
struct node **	list_to_array(struct list *l) {
    struct node ** nodeArr = malloc((sizeof(struct node)) * l->size);
    int counter = 0;
        
    struct node * traverse = l->head;
    while (traverse != NULL)
    {
        nodeArr[counter] = traverse;
        traverse = traverse->next;
        counter++;
    }

    return nodeArr;
}

/**
 * Sort list using qsort.
 *
 * This sorts the list using the qsort function from the standard C library.
 * @param   l	    List structure.
 * @param   f	    Node comparison function.
 */
void		list_qsort(struct list *l, node_compare f) {
    struct node ** array = list_to_array(l);
    
    qsort(array, l->size, sizeof(struct node *), f);
   
    l->head = array[0]; 
    struct node * traverse = l->head;
    for (size_t i = 1; i < l->size; i++)
    {
        traverse->next = array[i];
        traverse = traverse->next;      
    }
    traverse->next = NULL;
    l->tail = traverse;
    free(array);      
}

/**
 * Reverse list.
 *
 * This reverses the list.
 * @param   l	    List structure.
 */
void		list_reverse(struct list *l) {
    l->tail = l->head;
    l->head = reverse(l->head,NULL);   
}

/**
 * Reverse node.
 *
 * This internal function recursively reverses the node.
 * @param   curr    The current node.
 * @param   prev    The previous node.
 * @return  The new head of the singly-linked list.
 */
struct node*	reverse(struct node *curr, struct node *prev) {
    struct node * tail = curr;
    if (curr->next)
        tail = reverse(curr->next, curr);
    curr->next = prev;

    return tail;
}

/**
 * Sort list using merge sort.
 *
 * This sorts the list using a custom implementation of merge sort.
 * @param   l	    List structure.
 * @param   f	    Node comparison function.
 */
void		list_msort(struct list *l, node_compare f) {
    l->head = msort(l->head, f);
    
    struct node * traverse = l->head;
    while (traverse->next != NULL)
    {
        traverse = traverse->next;
    }   
    l->tail = traverse;
}

/**
 * Performs recursive merge sort.
 *
 * This internal function performs a recursive merge sort on the singly-linked
 * list starting with head.
 * @param   head    The first node in a singly-linked list.
 * @param   f	    Node comparison function.
 * @return  The new head of the list.
 */
struct node *	msort(struct node *head, node_compare f) {
    if (head == NULL || head->next == NULL)         // base case
        return head;
    
    struct node * left;
    struct node * right;
    split(head, &left, &right);
    
    left = msort(left, f);
    right = msort(right, f);
    
    head = merge(left, right, f); 
    return head;  
}

/**
 * Splits the list.
 *
 * This internal function splits the singly-linked list starting with head into
 * left and right sublists.
 * @param   head    The first node in a singly-linked list.
 * @param   left    The left sublist.
 * @param   right   The right sublist.
 */
void		split(struct node *head, struct node **left, struct node **right) {
    struct node * fast = head;
    struct node * slow = head;
    struct node * tail = head;

    while (fast && fast->next != NULL)
    {
        fast = fast->next->next;
        tail = slow;
        slow = slow->next;
    }
    *left = head;
    *right = slow;
    tail->next = NULL;
}

/**
 * Merge sublists.
 *
 * This internal function merges the left and right sublists into one ordered
 * list.
 * @param   left    The left sublist.
 * @param   right   The right sublist.
 * @param   f	    Node comparison function.
 * @return  The new head of the list.
 */
struct node *	merge(struct node *left, struct node *right, node_compare f) {
    struct node * final;
    struct node * head;

    if (f(&left,&right) < 0)
    {
        final = left;
        left = left->next;
    }
    else
    {
        final = right;
        right = right->next;
    }

    head = final;

    while (left != NULL && right != NULL)    
    {
        if (f(&left, &right) < 0)
        {
            final->next = left;
            left = left->next;
            final = final->next;                     
        }
        else
        {
            final->next = right;
            right = right->next;
            final = final->next;
        } 
    }
    if (left != NULL)
    {        
             // update leftovers
        while (left != NULL)
        {
            final->next = left;
            left = left->next;
            final = final->next;
        }
    }
    else
    {
        while (right != NULL)
        {
            final->next = right;
            right = right->next;
            final = final->next;
        }
    }
    return head;
}

/* vim: set sts=4 sw=4 ts=8 expandtab ft=c: */
