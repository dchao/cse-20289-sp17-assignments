/* lsort.c */

#include "list.h"

#include <stdio.h>
#include <stdlib.h>
#include <string.h>

/* Globals */

char * PROGRAM_NAME = NULL;

/* Functions */

void usage(int status) {
    fprintf(stderr, "Usage: %s\n", PROGRAM_NAME);
    fprintf(stderr, "  -n   Numerical sort\n");
    fprintf(stderr, "  -q   Quick sort\n");
    exit(status);
}

void lsort(FILE *stream, bool numeric, bool quicksort) {
    char line[BUFSIZ];
    struct list * newList = list_create();
    while (fgets(line,BUFSIZ,stream) != NULL)
    {
        list_push_back(newList, line);   
    }
    if (numeric)
    {
        if (quicksort)
            list_qsort(newList, node_compare_string);      
        else
            list_msort(newList, node_compare_number);
    }
    else
    {
        if (quicksort)
            list_qsort(newList, node_compare_string);
        else
            list_msort(newList, node_compare_string);  
    }
    struct node * traverse = newList->head;
    while (traverse->next != NULL)
    {
        printf(traverse->string);
        traverse = traverse->next;
    }
    list_delete(newList);
}

/* Main Execution */

int main(int argc, char *argv[]) {
    /* Parse command line arguments */
    int argind = 1;
    int numeric = false;
    int quicksort = false;
    while (argind < argc && strlen(argv[argind]) > 1 && argv[argind][0] == '-')
    {
        char * arg = argv[argind++];
        switch (arg[1])
        {
            case 'h':
                usage(1);
                break;
            case 'q':
                quicksort = true;
                break;
            case 'n':
                numeric = true;
                break;   
        }
        //argind++;
    }

    /* Sort using list */
    lsort(stdin, numeric, quicksort);       

    return EXIT_SUCCESS;
}

/* vim: set sts=4 sw=4 ts=8 expandtab ft=c: */
