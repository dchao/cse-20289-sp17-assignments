Homework 07 - Grading
====================

**Score**: 14/15

Deductions
----------
README
-.25 list_qsort should have space complexity of O(n)
-.25 list_reverse should have space and time complexity O(n)

test_lsort
-.25 some tests failed

benchmark
-.25 measure.c not included therefore script could not be tested

Comments
--------
make sure you submit all of the files needed to run the tests. Otherwise great job!
