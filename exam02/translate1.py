#!/usr/bin/env python2.7
import sys
import re
import string

# open /etc/passwd
with open('/etc/passwd') as f:
    text = f.readlines()

#cut -d : -f 5
fields = [line.split(':')[4] for line in text]
regex = '[uU]ser'

#grep
result = set([x for x in fields if re.search(regex,x)])

#tr 'A-Z' 'a-z'
for curstring in result:
    re.sub('[A-Z]', '[a-z]', curstring)

#sort
result = list(result)
result.sort()

for x in result:
    print x
