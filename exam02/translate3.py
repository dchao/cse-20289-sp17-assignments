#!/usr/bin/env python2.7
import subprocess

#ps aux
ps = subprocess.Popen(('ps', 'aux'), stdout=subprocess.PIPE)
output = ps.communicate()[0]
for line in output.split('\n'):
    #awk split
    line.split(" ")[0]

#sort
output = list(output)
output.sort()

#uniq
uniqOut = set()
for x in output:
    uniqOut.add(x)
finalOut = list(uniqOut)

#wc -1
wc = 0
for word in finalOut:
    wc = wc + 1

print wc


