#!/usr/bin/env python2.7
import re
import requests

#curl-s
text = str(requests.get('http://yld.me/aJt?raw=1').text).rstrip()
text = text.split('\n')

#grep
regex = r'[0-9]{3}-[0-9]{3}-[0-9]{3}[13579]'
result = [re.findall(regex,x)[0] for x in text if re.search(regex,x)]

for x in result:
    print x
