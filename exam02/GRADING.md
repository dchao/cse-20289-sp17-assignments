GRADING - Exam 02
=================

- Identification:   1.5
- Web Scraping:     3.25
- Generators:       5.25
- Concurrency:      5
- Translation:      5.5
- Total:	    20.5
