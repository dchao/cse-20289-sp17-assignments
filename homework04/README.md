Homework 04
===========
Activity 1: blend.py
1) The while loop was used to parse the command line arguments to always pop the flags first and then pop the option afterwards, similarly to have we have used it before in the last reading

2) tempfile.mkdtemp() was the command used to ceate a temporary directory and save it to a temporary path in the script.  The cleanup() was made to close the temp directory at the end of the function using atexit.register(cleanup)

3) I used requests.get to get the appropriate URL to output the right data by also using string formatting with {}.  Afterwards the image URL was found

4) requests.get was used again to download the portrait; afterwards I created a temporary file in the temp directory to place the data in

5) I used a for loop as well as the formatting command to create the composite images as they shifted.  If reverse was true or false would change whcih way the loop ran (i.e. forward or reverse) and then the images were added to the list

6) The final gif was created using the list in a string format in a final command

7) If the web response was not 200 the script exits early with a status error.  Also if there was any error from the arguments passed the script exits early.



Activity 2: reddit.py
1) The command line arguments were parsed in the way that we have been doing them: pop the first argument after checking if it is a flag, then popping the argument after to see what value was passed.  Teh regular expression passed was parsed with a simple else statement in the normal string of if statements to parse the flags

2) The JSON data was fetched using requests.get and then was iterated over using a for loop for the data in the JSON elements

3) Similarly, in the same for loop, the fields were filtered simply by passing them as values to variables that were used later for printing by accessing them from the dictionary of data we were iterating over at the time (the data of the article).  Ex: TITLEV = data['title'].  If the field was invalid (i.e. wasn't contained in the possible values passed fro the dictionary), then an error code was ent out.  The regular expression was used similarly to searching the for loop, but the command re.search was called to check if there was a match in the specific field.






