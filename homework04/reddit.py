#!/usr/bin/env python2.7
import requests
import os
import sys
import re

#Global Variables

FIELD	= 'title'
LIMIT	= 10
SUBREDDIT	= 'linux'
REGEX	= ""

#Functions

def usage(status=0):
    print '''Usage: reddit.py [ -f FIELD -s SUBREDDIT ] regex
    -f FIELD        Which field to search (default: title)
    -n LIMIT        Limit number of articles to report (default: 10)
    -s SUBREDDIT    Which subreddit to search (default: linux)'''.format(
        os.path.basename(sys.argv[0])
    )
    sys.exit(status)

# Parse command line options

args = sys.argv[1:]

while len(args) and args[0].startswith('-') and len(args[0]) > 1:
    arg = args.pop(0)
    
    if arg == '-h':
	usage(0)
    
    elif arg == '-f':
        #update FIELD
	arg = args.pop(0)
	FIELD = arg

    elif arg == '-n':
	#update LIMIT
	arg = int(args.pop(0))
	LIMIT = arg

    elif arg == '-s':
	#update SUBREDDIT
	arg = args.pop(0)
	SUBREDDIT = arg

    else:	#the regular expression
	arg = args.pop(0)
	REGEX = arg

#Main implementation

# begin the requests
headers  = {'user-agent': 'reddit-{}'.format(os.environ['USER'])}
URL = 'https://www.reddit.com/r/{}/.json'.format(SUBREDDIT)

response = requests.get(URL, headers=headers)

if response.status_code != 200:
    print "Error retrieivng code"
    sys.exit(0)

#variables used to print later
TITLEV = ''
AUTHORV = ''
pURLV = ''

data = response.json()
counter = 0
validField = 0

for resource in data['data']['children']:
    for fields in resource['data']:
	if re.search(FIELD, fields):
	    validField = validField + 1

    if (validField < 1):
	print "Error: invalid field"
	sys.exit(0)

    TITLEV = resource['data']['title']
    AUTHORV = resource['data']['author']
    pURLV = resource['data']['url']

    if re.search(REGEX, resource['data'][FIELD]):
	counter = counter + 1
	print counter, 'Title: ', TITLEV
    	print ' ', 'Author: ', AUTHORV
        print ' ', 'url: ', pURLV

    if (counter == LIMIT):
	break

