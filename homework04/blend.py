#!/usr/bin/env python2.7

import atexit
import os
import re
import shutil
import sys
import tempfile
import requests

os.environ['PATH'] = '~ccl/software/external/imagemagick/bin:' + os.environ['PATH']

# Global variables

REVERSE     = False
DELAY       = 20
STEPSIZE    = 5

# Functions

def usage(status=0):
    print '''Usage: {} [ -r -d DELAY -s STEPSIZE ] netid1 netid2 target
    -r          Blend forward and backward
    -d DELAY    GIF delay between frames (default: {})
    -s STEPSIZE Blending percentage increment (default: {})'''.format(
        os.path.basename(sys.argv[0]), DELAY, STEPSIZE
    )
    sys.exit(status)

#called at the end of the script to delete the temporary directory
def cleanup():
    shutil.rmtree(TEMP_PATH)

# Parse command line options

args = sys.argv[1:]

while len(args) and args[0].startswith('-') and len(args[0]) > 1:
    arg = args.pop(0)
    
    # TODO: Parse command line arguments
    if arg == '-h':
        usage(0)		

    elif arg == '-r':
        #blend forward and backward
        REVERSE = True				

    elif arg == '-d':
        #delay between frames
        arg = args.pop(0)
        DELAY = int(arg)	

    elif arg == '-s':
        #blending percentage increment ($2 is S)
        arg = args.pop(0)
        STEPSIZE = int(arg)
	
if len(args) != 3:
    usage(1)

netid1 = args[0]
netid2 = args[1]
target = args[2]

# Main execution

# TODO: Create workspace
TEMP_PATH =  tempfile.mkdtemp()

# TODO: Register cleanup

atexit.register(cleanup)

# TODO: Extract portrait URLs

request = (requests.get('https://engineering.nd.edu/profiles/{}'.format(netid1)))

if request.status_code == 200:
    #get the url for the image
    url1 = re.findall('https://.+{0}.+images.+png|https://.+{0}.+images.+jpeg'.format(netid1), request.text, flags=0)

else:
    sys.exit(1)

request = (requests.get('https://engineering.nd.edu/profiles/{}'.format(netid2)))

if request.status_code == 200:
    #get second image urll
    url2 = re.findall('https://.+{0}.+images.+png|https://.+{0}.+images.+jpeg'.format(netid2), request.text, flags=0)

else:
    sys.exit(1)

# TODO: Download portraits

request = requests.get(url1[0])
name1 = os.path.basename(url1[0])

#add the to our temp directory
name1 = os.path.join(TEMP_PATH,name1)

#create image file

with open(name1,'wb') as fs:

     fs.write(request.content)

request = requests.get(url2[0])

name2 = os.path.basename(url2[0])

#add to our temp directory path

name2 = os.path.join(TEMP_PATH,name2)

#create image file

with open(name2,'wb') as fs:

     fs.write(request.content)

# TODO: Generate blended composite images

#make a list of composites

comps = []

if REVERSE == False:

    start = 0

    stop = 100 + STEPSIZE

else:

    start=100


    stop=0 - STEPSIZE





#create composite files

for x in range(0, 100 + STEPSIZE,STEPSIZE):

    com_string = 'composite -blend {0} {1} {2} {3}/{4}-{5}.gif'.format(x,name1,name2,TEMP_PATH,x,target)

    os.system(com_string)

#add files to  composites

if REVERSE == False:

    for x in range(0, 100 + STEPSIZE,STEPSIZE):
        comps.append('{0}/{1}-{2}.gif'.format(TEMP_PATH,x,target))

    for x in range(100, 0 - STEPSIZE,(-1)*STEPSIZE):
        comps.append('{0}/{1}-{2}.gif'.format(TEMP_PATH,x,target))

else:

    for x in range(100, 0 - STEPSIZE,(-1)*STEPSIZE):
        comps.append('{0}/{1}-{2}.gif'.format(TEMP_PATH,x,target))

    for x in range(0, 100 + STEPSIZE,STEPSIZE):
        comps.append('{0}/{1}-{2}.gif'.format(TEMP_PATH,x,target))

# TODO: Generate final animation

com_string = 'convert -loop 0 -delay {0} {1} {2}'.format(DELAY,' '.join(comps),target)

os.system(com_string)
