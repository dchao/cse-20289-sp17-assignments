#!/usr/bin/env/ python2.7

import os
import sys
import subprocess

counter = 1
size = 1
load = .5


while size <= 1000000000:
    line = "shuf -i1-{0} -n {0} | ./measure ./freq -l {1} > /dev/null".format(size, load)
    result = subprocess.check_output(line, shell=true)
    result.split()
    time = output[0]
    memory = output[2]
    print "{0} {1} {2} {3}".format(str(size), str(load), time, memory)
    if counter % 8 = 0:
        size *= 10
    if load = .5:
        load = .75
    if load = .75:
        load = .9
    if load = .9:
        load = 1.0
    if load = 1.0:
        load = 2.0
    if load = 2.0:
        load = 4.0
    if load = 4.0:
        load = 8.0
    if load 8.0:
        load = 16.0
# vim: set sts=4 sw=4 ts=8 expandtab ft=c: #
