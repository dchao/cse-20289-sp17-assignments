/* freq.c */

#include "map.h"

#include <stdio.h>
#include <stdlib.h>
#include <string.h>

/* Globals */

char * PROGRAM_NAME = NULL;
DumpMode FORMAT = VALUE_KEY;
double LOAD_FACTOR = 0;
/* Functions */

void usage(int status) {
    fprintf(stderr, "Usage: %s\n", PROGRAM_NAME);
    fprintf(stderr, "    -f FORMAT        Output format (KEY, KEY_VALUE, VALUE, VALUE_KEY)\n");
    fprintf(stderr, "    -l LOAD_FACTOR   Load factor for hash table\n");
    exit(status);
}

void freq_stream(FILE *stream, double load_factor, DumpMode mode) {
    char buffer[BUFSIZ];
    Map * m = map_create(5, load_factor);
    Value curVal;
    
    while (fscanf(stream, "%s", buffer) != EOF)
    {
        Entry * temp = map_search(m, buffer);
        if (temp != NULL)
        {
            curVal = temp->value;
            curVal.number = curVal.number+1;
        }
        else
        {
            curVal.number = 1;
        }
        map_insert(m, buffer, curVal, NUMBER);
    }      
    map_dump(m, stdout, mode);
    map_delete(m);
}

/* Main Execution */

int main(int argc, char *argv[]) {
    /* Parse command line arguments */
    int argind = 1;
    int numeric = false;
    int quicksort = false;
    while (argind < argc && strlen(argv[argind]) > 1 && argv[argind][0] == '-')
    {
        char * arg = argv[argind++];
        switch (arg[1])
        {
            case 'h':
                usage(1);
                break;
            case 'f':
                if (strcmp(argv[argind], "KEY") == 0)
                    FORMAT = KEY;
                else if (strcmp(argv[argind], "VALUE") == 0)
                    FORMAT = VALUE;
                else if (strcmp(argv[argind], "KEY_VALUE") == 0)
                    FORMAT = KEY_VALUE;
                else if (strcmp(argv[argind], "VALUE_KEY") == 0)
                    FORMAT + VALUE_KEY;
                argind++;
                break;
            case 'l':
                LOAD_FACTOR = atof(argv[argind++]);               
                break;    
        }       
        //argind++;
        //    }   
        //        
        //
    /* Compute frequencies of data from stdin */
    freq_stream(stdin, LOAD_FACTOR, FORMAT);   
    return EXIT_SUCCESS;
}

/* vim: set sts=4 sw=4 ts=8 expandtab ft=c: */
