Grading - Homework 08
=====================

Activity 1
----------

- 0.25  Makefile doesn't suppress command tracing
- 0.25  Objects depend on headers

- 3.0   Missing responses

8.5 / 12

Activity 2
----------

- 2.5   Doesn't compile, missing responses

0.5 / 3

Total
-----

9 / 15
