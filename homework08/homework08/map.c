/* map.c: separate chaining hash table */

#include "map.h"

/**
 * Create map data structure.
 * @param   capacity        Number of buckets in the hash table.
 * @param   load_factor     Maximum load factor before resizing.
 * @return  Allocated Map structure.
 */
Map *	        map_create(size_t capacity, double load_factor) {
    Map * newMap = malloc(sizeof(Map));
    if (newMap == NULL)
    {
        printf("Error: could not allocate new map");
        return NULL;
    }
    if (capacity == 0)
        newMap->capacity = DEFAULT_CAPACITY;
    else
        newMap->capacity = capacity;
    
    if (load_factor <= 0)
        newMap->load_factor = DEFAULT_LOAD_FACTOR;
    else
        newMap->load_factor = load_factor;
    
    newMap->size = 0;
    newMap->buckets = calloc(newMap->capacity, sizeof(Entry));          
    return newMap;
}

/**
 * Delete map data structure.
 * @param   m               Map data structure.
 * @return  NULL.
 */
Map *	        map_delete(Map *m) {
    
    return NULL;
}

/**
 * Insert or update entry into map data structure.
 * @param   m               Map data structure.
 * @param   key             Entry's key.
 * @param   value           Entry's value.
 * @param   type            Entry's value's type.
 */
void            map_insert(Map *m, const char *key, const Value value, Type type) {
    double load_factor = (double)m->size / m->capacity;
    if (load_factor > m->load_factor)
        map_resize(m, m->capacity * 2);

    uint64_t bucket = fnv_hash(key, strlen(key)) % m->capacity;
    //Entry * e = entry_create(key, value, NULL, type);
    Entry * temp = m->buckets[bucket].next;             //error
    while (temp != NULL)
    {
        if (strcmp(key, temp->key) == 0)                //error
        {
            entry_update(temp, value, type);       
            return;
        }
        temp = temp->next;
    }
        Entry * e = entry_create(key, value, NULL, type);
        e->next = m->buckets[bucket].next;              //error
        m->buckets[bucket].next = e;                    //error
        m->size += 1;
}

/**
 * Search map data structure by key.
 * @param   m               Map data structure.
 * @param   key             Key of the entry to search for.
 * @param   Pointer to the entry with the specified key (or NULL if not found).
 */
Entry *         map_search(Map *m, const char *key) { 
    uint64_t bucket = fnv_hash(key, strlen(key)) % m->capacity;
    Entry * searchE = m->buckets[bucket].next;
    while (searchE != NULL)
    {
        if (strcmp(searchE->key, key) == 0)
        {
            return searchE;
        }
        searchE = searchE->next;
    }
    return NULL;
}

/**
 * Remove entry from map data structure with specified key.
 * @param   m               Map data structure.
 * @param   key             Key of the entry to remove.
 * return   Whether or not the removal was successful.
 */
bool            map_remove(Map *m, const char *key) {
    uint64_t bucket = fnv_hash(key, strlen(key)) % m->capacity;
    Entry * previous = &(m->buckets[bucket]);
    Entry * current = m->buckets[bucket].next;
    while (current != NULL)
    {
        if (strcmp(key, current->key) == 0)
        {
            previous->next = current->next;
            entry_delete(current, false);
            m->size -= 1;
            return true;
        }
        previous = previous->next; 
        current = current->next;
    }
    return false;
}

/**
 * Dump all the entries in the map data structure.
 * @param   m               Map data structure.
 * @param   stream          File stream to write to.
 * @param   mode            Dump mode to use.
 */
void		map_dump(Map *m, FILE *stream, const DumpMode mode) {
    for (int i = 0; i < m->size; i++)
    {
        Entry * e = m->buckets[i].next;
        while (e != NULL)
        {
            entry_dump(e, stream, mode);
            e = e->next;
        }
    }
}

/**
 * Resize the map data structure.
 * @param   m               Map data structure.
 * @param   new_capacity    New capacity for the map data structure.
 */
void            map_resize(Map *m, size_t new_capacity) {
    Entry * old_buckets = m->buckets;
    size_t old_capacity = m->capacity;
    Entry * new_buckets = calloc(new_capacity, sizeof(Entry));

    for (size_t i = 0; i < old_capacity; i++)
    {
        Entry * temp = old_buckets[i].next;
        while (temp != NULL)
        {
            uint64_t new_bucket = fnv_hash(temp->key, strlen(temp->key)) % (int) new_capacity;
            Entry * append = temp;
            append->next = new_buckets[new_bucket].next;
            new_buckets[new_bucket].next = append;
            temp = temp->next;
        } 
    }
    
    m->buckets = new_buckets;
    m->capacity = new_capacity;

}



/* vim: set sts=4 sw=4 ts=8 expandtab ft=c: */
