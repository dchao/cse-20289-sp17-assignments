#!/usr/bin/env python2.7

import functools
import hashlib
import itertools
import multiprocessing
import os
import string
import sys

# Constants

ALPHABET    = string.ascii_lowercase + string.digits
ARGUMENTS   = sys.argv[1:]
CORES       = 1
HASHES      = 'hashes.txt'
LENGTH      = 1
PREFIX      = ''

# Functions

def usage(exit_code=0):
    print '''Usage: {} [-a alphabet -c CORES -l LENGTH -p PATH -s HASHES]
    -a ALPHABET Alphabet to use in permutations
    -c CORES    CPU Cores to use
    -l LENGTH   Length of permutations
    -p PREFIX   Prefix for all permutations
    -s HASHES   Path of hashes file'''.format(os.path.basename(sys.argv[0]))
    sys.exit(exit_code)

def md5sum(s):
    ''' Generate MD5 digest for given string.

    >>> md5sum('abc')
    '900150983cd24fb0d6963f7d28e17f72'

    >>> md5sum('wake me up inside')
    '223a947ce000ce88e263a4357cca2b4b'
    '''
    # TODO: Implement
    pass
    md5 = hashlib.md5()
    md5.update(s)
    output = md5.hexdigest()
    return output

def permutations(length, alphabet=ALPHABET):
    ''' Yield all permutations of alphabet of specified length.

    >>> list(permutations(1, 'ab'))
    ['a', 'b']

    >>> list(permutations(2, 'ab'))
    ['aa', 'ab', 'ba', 'bb']

    >>> list(permutations(1))       # doctest: +ELLIPSIS
    ['a', 'b', ..., '9']

    >>> list(permutations(2))       # doctest: +ELLIPSIS
    ['aa', 'ab', ..., '99']
    '''
    # TODO: Implement
    pass
    for perm in itertools.product(alphabet, repeat=length):
        yield ''.join(perm)

def smash(hashes, length, alphabet=ALPHABET, prefix=''):
    ''' Return all password permutations of specified length that are in hashes

    >>> smash([md5sum('ab')], 2)
    ['ab']

    >>> smash([md5sum('abc')], 2, prefix='a')
    ['abc']

    >>> smash(map(md5sum, 'abc'), 1, 'abc')
    ['a', 'b', 'c']
    '''
    # TODO: Implement
    pass
    
    perms = permutations(length, alphabet)
    fullPerms = [prefix + rest for rest in perms]
    finalMatched = []

    for hashMatch in fullPerms:
        if md5sum(hashMatch) in hashes:
            finalMatched.append(hashMatch)

    return finalMatched

# Main Execution

if __name__ == '__main__':
    # TODO: Parse command line arguments
    args = sys.argv[1:]

    while len(args) and args[0].startswith('-') and len(args[0]) > 1:
        arg = args.pop(0)

        if arg == '-h':
            usage(0)

        elif arg == '-c':
        #update CORES
            arg = int(args.pop(0))
            CORES = arg

        elif arg == '-l':
        #update LENGTH
            arg = int(args.pop(0))
            LENGTH = arg

        elif arg == '-s':
        #update HASHES
            arg = args.pop(0)
            HASHES = arg

        elif arg == '-p':
        #update PREFIX
            arg = args.pop(0)
            PREFIX = arg

        elif arg == '-a':
            arg = args.pop(0)
            ALPHABET = arg

    # TODO: Load hashes set
    hashes = set([line.rstrip('\n') for line in open(HASHES)])

    # TODO: Execute smash function to get passwords
    splitLength1 = LENGTH/2
    splitlength2 = LENGTH - splitLength1
    notdone = [PREFIX + pswd for pswd in permutations(splitlength2, ALPHABET)]

    #Note: I was so damn lost here until I realized what itertools did, I honestly didn't know if it was working or not for a while
    #partials/multicores (get to this immediately)
    if CORES > 1 and LENGTH > 1:    
        subsmash = functools.partial(smash, hashes, splitLength1, ALPHABET)            #just smash hashes for splitlength
        pool = multiprocessing.Pool(CORES)
        # need to chain from pool...?

        passwords = itertools.chain.from_iterable(pool.imap(subsmash, notdone))
    else:                                                                               #normal, dont worry about the partials
        passwords = smash(hashes, LENGTH, ALPHABET, PREFIX)

    # TODO: Print passwords
    pass
    for password in passwords:
        print password

# vim: set sts=4 sw=4 ts=8 expandtab ft=python:



