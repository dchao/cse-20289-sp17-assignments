#!/usr/bin/env python2.7
import sys

items = {}
newlist = []
for line in sys.stdin:
    KEY, VAL  = line.split('\t', 1)

    if items.has_key(KEY) == False:
        items[KEY] = set()
    items[KEY].add(str(VAL).rstrip())

for KEY, VAL in sorted(items.items()):
    print '{}\t{}'.format(KEY, ' '.join([i for i in sorted(VAL, key = float) if i != '']))