#!/usr/bin/env python2.7

import sys
import string

#MAP

count = 0
alphabet = 'abcdefghijklmnopqrstuvwxyz-'
newlist = [] 

for curline in sys.stdin:
    count = count + 1
    for word in curline.strip().split():
        newlist.append('{}\t{}'.format(''.join([make.lower() for make in word if make.lower() in alphabet]), count))

for item in newlist:
    print item
