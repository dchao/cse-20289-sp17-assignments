GRADING.md
==========

hulk.py
--------
looks good!

tests 
-----
good!

Deadpool
--------
* `-1`: missing passwords

README.md
---------
* `-.25`: missing quesion 2

iv_map.py
---------
* `-.5`: Doesn't remove punctuation (besides -)

iv_reduce.py
------------
* `-.5`:  Doesn't aggregate line numbers for each word

tests
-----
*`-1`: most tests fail


**Total: 11.25/15**