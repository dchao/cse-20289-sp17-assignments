Homework 05
===========

actvity 1: hulk.py
1) a) The candidate password combinations were generated mainly in the 
	permutations function which generated all possible combinations 
	given our alphabet

   b)candidates were filtered in smash where only hashes that matched 
   our given hash list were yielded for our list.  If the hash matched it was 
   yielded (with a simple if statement).

   c) Processing on multiple cores was handled by an if statement that 
   would call on functools.partial if the core variable was greater than 
   1
   
   d) the code was verified using the testing mechanisms provided by 
   professor bui
   
2) processes			time
	1
	2
	4
	8
	16
	
3) more complex alphabet: in generating the permutations for possible 
passwords we need to cover all the leters of the alphabet per slot in 
the password: so while with the same number of characters for a longer 
password requires more permutations total, a larger character range 
creates mre permutations per each slot of the password, so a 6 letter 
password will take longer, 7, 8, etc.  These permutations would be less 
complex if the password length alone would be longer since we are not 
changing the set of characters we need to compute permutations for.

activity 2: inverted index
1) a) iv_map keeps count by using a count variable that increases per 
   line in the for loop for the tandard in.
   b) in the second for loop the line is stripped and split as well as 
   formatted within the for loop to remove undesirable characters
   
2) a) iv_reduce aggregates the results within the for loop by appending 
   the value at the key index for items.
   b) The results are outputted properly by sorting it with a list 
   comprehension while it's being printed.
