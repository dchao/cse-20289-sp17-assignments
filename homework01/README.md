Homework 01
===========
A1) 1a) The private directory has the "least" amount of rights in that it 
	    does not have rights for nd_campus or for system:authuser.  The 
	    Difference between the Public and home directories is the 
	    addition of "rk" permissions for nd_campus and asystem:authuser 
	    in the Public directory.
	1b) The Home directory is able to lookup files on nd_campus and 
	    system:authuser while the Private directory has read and lock 
	    permissions as well in both of those.
	2a) Permission was denied from the directory so I was unable to 
		create the file
	2b) The ACLs take precedence

A2) | Command | Elapsed Time |
	--------------------------
	| cp ...  | 0.039s		 |
	| mv ...  | 0.001s		 |
	| mv ...  | 0.025s		 |
	| rm ...  | 0.001s		 |
	
	1) Renaming takes less time because it is moving within the same 
	   directory while moving it to a different directory requires 
	   navigating to the new directory and then moving the directory 
	   over
	2) Removing simply deletes the files from the memory areas it has 
       been allocated to, thus deleting it would take less time then 
       moving it since it doesn't need to navigate but simply delete it

A3) 1) bc < math.txt
	2) bc < math.txt > result.txt
	3) bc < math.txt > result.txt 2> /dev/null
	4) cat math.txt | bc.  This is considered less effiecient because it 
	   requires calling 2 processes as opposed to 1

A4) 1) 0 accounts in the passwd file have /sbin/nologin as their shell
	2) 25 users--> users | wc -w
	3) gconf, selinux, brltty, pki, termcap-->
	   du -hsx * 2> /dev/null | sort -rh | head -5
	4) 2--> ps aux | grep '/bin/bash' | wc -l 

A5) 1a) Control-C failed to work and I was taunted.  kill failed to kill 
		the process after it was suspended by control-z.  
	1b) Control-z, then ps to find the PID, then used kill -9
	2a) kill -KILL $(ps aux | grep 'dchao'|grep 'TROLL' | awk '{print $2}')
	2b) kill -9 PID where the PID was found using the top command
	3)  Passing the -1 signal when using the kill command aborted me 
		from the student machine in all of my terminals (I don't think 
		that was part of TROLL but it still happened)
