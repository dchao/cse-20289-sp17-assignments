Homework 06
===========

Activity 1

1) string_reverse_words works by utlizing the string_reverse_range function internally and on each individual word. First the entire string itself is reversed, reversing the order of the words. Iterating over the string with 2 pointers, if the first pointer points to a vlaue other than space, the pointer increments. The second pointer will then be set to the value of first space after the word. Next, string_reverse_range  will be called on the range from the first pointer to the second pointer - 1 (to have it be just the word, since the second pointer stops at the first space), putting the owrds back in proper english. The time complexity for this function is: O(n^2); this is because it has a loop inside a loop. In terms of space it is O(1), of constant space.

2) string_translate runs through the string using two loops. It utilizes two pointers to compare the values of the string to translate against the source set. If the values of the first pointer match the character in the source set, it swaps this with the corresponding character in the target set.  The time complexity for this function is: O(n^2); this is because it has a loop inside a loop. In terms of space it is O(1), of constant space.

3) string_to_integer takes in numerical base, which is used in computing the final value of the integer. A pointer is instantiated at the end of the string. Starting from the back, the first check is to see if the character is an integer or an Alphanumeric character in hexadecimal notation. If it is an alphabetical letter in hex(scanned for both lowercase and uppercase), this value is then multiplied by a value (baseMult) which is multiplied by the base passed to the function per each iteration. If the value is an integer, the same is done with the addition of subtracting the ascii value of '0' (48) to convert into an integer.  The pointer decrements each time to iterate backwards over the string so we go in increasing base value (think binary). the final value is returned as the sum of all of these individual characters as an integer.  The time complexity for this function is: O(n); a single loop. In terms of space it is O(1), of constant space.


4) GCC's default is to produce dynamic executables which require the presence of libraries at run time. thus we need shared libraries that can be linked to other applications. In the case of static executables static libraries are required; these are created differently.  Static executables dont rely on shared libraries; with a shared library, the library is run every time we run the executable. this is not the same for a static library.

-rw-r--r--  1 dchao dip  12K Mar 24 12:50 libstringutils.a
-rwxr-xr-x  1 dchao dip  14K Mar 24 12:50 libstringutils.so

the shared library (libstringutils.so) is larger.


Activity 2

1) I iterated through argv[] using argind,an index which increases with every iteration of the while loop; within this a case statement was used to handle each flag.  The stream was passed by utilizing a while loop in conjunction with fgets: this stored the stream in a char array (buffer) and would subsequenlty call the specified function(s) to the stream from the flags passed. This was done by calling the different functions that I made in stringutils.c, which were handled by a series of in statements based on the bitmask.  The bitmask was used by creating an enumerate which assigned bit values to each different flag. In the case statement which parsed the command line arguments the mode (part of the bitmask) was updated based on the flag which specified a function. the mode would then subsequently be passed to the translation function.

Static executables require static libraries whereas dynamic executables require shared libraries.  Static libraries are stored entirely within the executable while shared libraries are linked to the executable.

-rwxr-xr-x  1 dchao dip  14K Mar 24 12:50 translate-dynamic
-rw-r--r--  1 dchao dip  11K Mar 24 12:50 translate.o
-rwxr-xr-x  1 dchao dip 885K Mar 24 12:50 translate-static

static executables require static library which is stored entirely in the executable, making it portable. dynamic exectables require that the executable search through the shared libraries every time the executable is called.  This causes the executable to be smaller in memory because it does not have the library in it

Yes, the command works.

./translate-dynamic: error while loading shared libraries: libstringutils.so: cannot open shared object file: No such file or directory
no, in order for the executable to run the executable needs to search through all the libraries.
