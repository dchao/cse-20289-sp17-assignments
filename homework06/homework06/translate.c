/* translate.c: string translator */

#include "stringutils.h"

#include <errno.h>
#include <stdio.h>
#include <stdlib.h>
#include <string.h>

/* Globals */

char *PROGRAM_NAME = NULL;
enum {
    STRIP = 1<<1,
    REVERSE = 1<<2,
    REVWORDS = 1<<3,
    LOWER = 1<<4,
    UPPER = 1<<5
};

/* Functions */

void usage(int status) {
    fprintf(stderr, "Usage: %s SOURCE TARGET\n\n", PROGRAM_NAME);
    fprintf(stderr, "Post Translation filters:\n\n");
    fprintf(stderr, "   -s     Strip whitespace\n");
    fprintf(stderr, "   -r     Reverse line\n");
    fprintf(stderr, "   -w     Reverse words in line\n");
    fprintf(stderr, "   -l     Convert to lowercase\n");
    fprintf(stderr, "   -u     Convert to uppercase\n");
    exit(status);
}

void translate_stream(FILE *stream, char *source, char *target, int mode) {
    /* TODO */
    char buffer[BUFSIZ];
    while (fgets(buffer, BUFSIZ, stream))
    {
        string_chomp(buffer);
        string_translate(buffer, source, target);
        if (mode & STRIP)
        {
           string_strip(buffer);
           // fputs(buffer, stdout);
        }
        if (mode & REVERSE)
        {
            string_reverse(buffer);
          //  fputs(buffer, stdout);
        }
        if (mode & REVWORDS)
        {
            string_reverse_words(buffer);
          //  fputs(buffer, stdout);
        }
        if (mode & LOWER)
        {
            string_lowercase(buffer);
          //  fputs(buffer, stdout);
        }
        if (mode & UPPER)
        {
            string_uppercase(buffer);
          //  fputs(buffer, stdout);
        }
        string_translate(buffer, source, target);
    } 
    fputs(buffer, stdout);
    printf("\n");
}

/* Main Execution */

int main(int argc, char *argv[]) {
    /* Parse command line arguments */
    PROGRAM_NAME = argv[0];
    char *TARGET = NULL;
    char *SOURCE = NULL;
    int argind = 1;
    int mode = 0;
    while (argind < argc && strlen(argv[argind]) > 1 && argv[argind][0] == '-')
    {
        char *arg = argv[argind++];
        switch(arg[1])
        {
            case 'h':
                usage(0);
                break;
            case 's':
                mode |= STRIP;               
                break;
            case 'r':
                mode |= REVERSE;
                break;
            case 'w':
                mode |= REVWORDS;       
                break;
            case 'l':
                mode |= LOWER;
                break;
            case 'u':
                mode |= UPPER;
                break;
            default:
                usage(1);
                break;       
        }
    }
    if ((argc - argind) > 1)
    {
        SOURCE = argv[argind++];
    }
    if ((argc - argind) > 0)
    {
        TARGET = argv[argind++];
    }
    /* Translate Stream */
    translate_stream(stdin, SOURCE, TARGET, mode);
 
    return EXIT_SUCCESS;
}

/* vim: set sts=4 sw=4 ts=8 expandtab ft=c: */
