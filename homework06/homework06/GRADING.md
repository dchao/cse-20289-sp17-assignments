GRADING.md
==========

Activity 1
----------

Makefile
--------
looks good.

stringutils.c
-------------
looks good

tests
-----
passes all tests

README.md
---------
good

Activity 2
----------

Makefile
--------
looks good!

translate.c
-----------
code looks good, but there must be something wrong here bc you failed 2 tests. I couldn't find it though, so no points off.

tests
-----
* `-.5`: fails 2 tests

README.md
---------
good


**Total: 14.5/15**