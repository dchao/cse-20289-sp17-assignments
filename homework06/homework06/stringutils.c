/* stringutils.c: String Utilities */

#include "stringutils.h"

#include <ctype.h>
#include <string.h>

/**
 * Convert all characters in string to lowercase.
 * @param   s       String to convert
 * @return          Pointer to beginning of modified string
 **/
char *	string_lowercase(char *s) {
    for (char *p = s; *p != '\0'; p++)
    {
        if (*p > 64 && *p < 91)
        {
            *p = *p + 32;
        }
    }

    return s;
}

/**
 * Convert all characters in string to uppercase.
 * @param   s       String to convert
 * @return          Pointer to beginning of modified string
 **/
char *	string_uppercase(char *s) {
    for (char *p = s; *p != '\0'; p++)
    {
        if (*p > 96 && *p < 123)
        {
            *p = *p - 32;
        }
    }

    return s;
}

/**
 * Returns whether or not the 's' string starts with given 't' string.
 * @param   s       String to search within
 * @param   t       String to check for
 * @return          Whether or not 's' starts with 't'
 **/
bool	string_startswith(char *s, char *t) {
    if (strlen(t) > strlen(s))
        return false;
    for (char *p = t; *p != '\0'; p++)
    {
        if (*p != *s)
            return false; 
        s++;
    }
    return true;
}

/**
 * Returns whether or not the 's' string ends with given 't' string.
 * @param   s       String to search within
 * @param   t       String to check for
 * @return          Whether or not 's' ends with 't'
 **/
bool	string_endswith(char *s, char *t) {
    s = s + strlen(s) - strlen(t);
    if (strlen(s) < strlen(t))
        return false;
    for (char *p = t; *p != '\0'; p++)
    {
        if (*p != *s)
            return false;
        s++;
    }
    return true;
}

/**
 * Removes trailing newline (if present).
 * @param   s       String to modify
 * @return          Pointer to beginning of modified string
 **/
char *	string_chomp(char *s) {
    char *match = s + strlen(s) - 1;;
    if (match[0] == '\n')
    {
        *match = '\0';
    }

    return s;
}

/**
 * Removes whitespace from front and back of string (if present).
 * @param   s       String to modify
 * @return          Pointer to beginning of modified string
 **/
char *	string_strip(char *s) {
    string_chomp(s);
    
    char *r = s;
    char *w = s;
    
    if (strlen(s) == 0)
        return s;

    //front
    while (*r == ' ')
    {
        r++;
    }
    while (*r != '\0')
    {
        *w = *r;
        r++;
        w++;
    }
    
    //back
    *w = *r;
    r = s + strlen(s) - 1;
    while (*r == ' ')
    {
        r--;
        *(r+1) = '\0';
    }
    
    return s;
}

/**
 * Reverses a string given the provided from and to pointers.
 * @param   from    Beginning of string
 * @param   to      End of string
 * @return          Pointer to beginning of modified string
 **/
static char *	string_reverse_range(char *from, char *to) {
    if (strlen(from) == 0)
        return from;   
    char *back = to;
    for (char *p = from; p < back; p++)
    {
        char temp = *p;
        *p = *back;
        *back = temp;
        back--;   
    }

    return from;
}

/**
 * Reverses a string.
 * @param   s       String to reverse
 * @return          Pointer to beginning of modified string
 **/
char *	string_reverse(char *s) {
    string_chomp(s);
    char *back = s + strlen(s) - 1;
    s = string_reverse_range(s, back);   
    return s;
}

/**
 * Reverses all words in a string.
 * @param   s       String with words to reverse
 * @return          Pointer to beginning of modified string
 **/
char *	string_reverse_words(char *s) {
    string_reverse(s);
    char *from = s;
    char *to = s;

    while (*from != '\0')
    {
        while (*from == ' ' && *from != '\0')
        {
            from++;   
        }
        to = from;
        while (*(to+1) != ' ' && *(to+1) != '\0')
        {
            to++;
        }
        string_reverse_range(from, to);    
        from = ++to;
    }
    
    return s;    
}

/**
 * Replaces all instances of 'from' in 's' with corresponding values in 'to'.
 * @param   s       String to translate
 * @param   from    String with letter to replace
 * @param   to      String with corresponding replacment values
 * @return          Pointer to beginning of modified string
 **/
char *	string_translate(char *s, char *from, char *to) {
    string_chomp(s);
    if (strlen(from) != strlen(to))
        return s;
    char *fromDex = from;
    char *toDex = to;
    for (fromDex; *fromDex != '\0'; fromDex++)
    {
        for (char *p = s; *p != '\0'; p++)
        {
            if (*fromDex == *p)
            {
                *p = *toDex;
            }
        }
        toDex++;     
    }
    return s;
}

/**
 * Converts given string into an integer.
 * @param   s       String to convert
 * @param   base    Integer base
 * @return          Converted integer value
 **/
int	string_to_integer(char *s, int base) {
    int sum = 0;
    int baseMult = 1;
    int dig = 0;
    char *rev = s + strlen(s) - 1;
    while (*rev)
    {       
        if (*rev > 47 && *rev < 58)
        {
            dig = *rev - 48;
        }
        else if(toupper(*rev) > 64 && toupper(*rev) < 71)
        {
            dig = toupper(*rev) - 55;
        }
        
        sum += dig * baseMult;
        baseMult *= base;
        rev--;
    }
    
    return sum;
}

/* vim: set sts=4 sw=4 ts=8 expandtab ft=c: */
