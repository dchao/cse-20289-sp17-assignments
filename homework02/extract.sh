#!/bin/bash

if [ $# == 0 ]; then
	echo "Error: must pass argument of an archive to unzip"
	echo "Usage: extract.sh archive1 archive2 ..."
else
	for archive in $@
	do
		ext=${archive##*\.}

		case $ext in
			tgz|gz)
			tar -xvsf $archive
			;;
			tbz|bz2)
			tar xvjf $archive
			;;
			xz)
			unxz $archive
			tar -xf $archive
			;;
			txz)
			xz -d < $archive | tar xvf -
			;;
			zip|jar)
			unzip $archive
			;;
		esac

		shift
	done
fi

