GRADING.md
==========

Activity 1
==========

extract.sh
----------
Script looks good! You've got most of the basics.


test_extract.sh
---------------
`-.5` There is a bug with `tar.gz` and `.tgz` files. Your flags are a little off, if you did `tar xzvf $archive` instead of `tar -xvsf $archive` you would've passed all the tests.


Task 3
------
good!


Activity 2
==========

fortune.sh
----------
good!


test_fortune.sh
---------------
good!


Task 3
------
good!


Activity 3
==========

`-.75` 5: Didn't talk about accessing the doorman with `curl http://xavier.h4x0r.space:9876/netid/passcode` or talking to `BOBBIT`.

`-.5` 6: Didn't talk to `BOBBIT` in slack using `!verify netid passcode`





**Total: 13.25/15**