Homework 02
===========

A1) extract.sh

1) Checked having no arguments by using an if statement testig the 
special 
variable $#: if $# was equal to zero then the usage message was passed, 
if not the archives were unzipped

2) I used a case statement to determine which command to use for the 
different argument types (i.e. the different extensions)

3) The most challenging part was to actually get all the commands to 
properly unzip based on their type: even when using the right command I 
would still get errors when trying to unzip the archives properly.

A2) fortune.sh

1) Random messages were displayed to the user using the shuf command and here docs 
to randomly pick a phrase from that group and then send it out using cowsay after 
being piped through head to isolate just one

2) The signals were handled via trap and a function that catches the signals

3) The input is read from the user using the read command

4) the most challenging part of this script was definitely generating the random 
sayings using shuf and here documents but once I figured that out after a lot of 
unnecessary reading the rest went swiftly

A3) the Oracle

First step was to scan for an HTTP port on xavier.h4x0r.space

$ nmap -Pn xavier.h4x0r.space
Host is up (0.00048s latency).
Not shown: 946 filtered ports, 50 closed ports
PORT     STATE SERVICE
22/tcp   open  ssh
8888/tcp open  sun-answerbook
9111/tcp open  DragonIDSConsole
9876/tcp open  sd

2 ports in the 9000-9999 range: DragonIDSConsole and sd

Next i tried to access the server

bash-4.1$ ftp xavier.h4x0r.space 9111
Connected to xavier.h4x0r.space.
 ________________________ 
 < Hello, who may you be? >
 Name (xavier.h4x0r.space:dchao): ah
  ------------------------ 
  Login failed.
  ftp> quit
  
next i tried the other server but used curl instead

curl xavier.h4x0r.space:9876
 ________________________________________ 
 / Halt! Who goes there?                  \
 |                                        |
 | If you seek the ORACLE, you must come  |
 | back and _request_ the DOORMAN at      |
 | /{NETID}/{PASSCODE}!                   |
 |                                        |
 | To retrieve your PASSCODE you must     |
 | first _find_ your LOCKBOX which is     |
 | located somewhere in                   |
 | ~pbui/pub/oracle/lockboxes.            |
 |                                        |
 | Once the LOCKBOX has been located, you |
 | must use your hacking skills to        |
 | _bruteforce_ the LOCKBOX program until |
 | it reveals the passcode!               |
 |                                        |
 \ Good luck!                             /
  ---------------------------------------- 
  
Then I tried to find my lockbox using:

 find ~pbui/pub/oracle/lockboxes | grep dchao
 
one of the things it returned was:

/afs/nd.edu/user15/pbui/pub/oracle/lockboxes/e5cd1b09/54c64998/3c787168/1a4c42f2/dchao.lockbox

Next i tried to find my password by bruteforcing the lockbox with the 
string command using a script
for password in $( strings dchao.lockbox)
> do 
> ./dchao.lockbox $password 
> done

0ba0a1acfc3408484e59bab4208b4f10
Then I used nc xavier.h4x0r.space 9111
entered my NETID and then my message and finished

