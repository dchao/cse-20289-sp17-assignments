#!/bin/bash

early_exit() {
	cat <<EOF | /afs/nd.edu/user15/pbui/pub/bin/cowsay -f tux
	Fine leave early I dont care
	youre the one who wont know the future
EOF
	exit 1
}

trap early_exit INT HUP TERM

valid=0
/afs/nd.edu/user15/pbui/pub/bin/cowsay -f tux<<EOF
	Hey $USER, what do you need me to answer today?
EOF

read question

while [ $valid -eq 0 ]
do
	if [[ -z "$question" ]]; then
		cat <<EOF | /afs/nd.edu/user15/pbui/pub/bin/cowsay -f tux
		Dont just type blanks even tho I know what your question is
		Ask away
EOF
		read question
	else
		shuf <<EOF | head -n 1 | /afs/nd.edu/user15/pbui/pub/bin/cowsay -f tux
		It is certain
		It is decidedly so
		Without a doubt
		Yes, definitely
		You may rely on it
		As I see it, yes
		Most likely
		Outlook good
		Yes
		Signs point to yes
		Reply hazy try again
		Ask again later
		Better not tell you now
		Cannot predict now
		Concentrate and ask again
		Don't count on it
		My reply is no
		My sources say no
		Outlook not so good
		Very doubtful
EOF
		valid=1
	fi
done
