Reading 03
==========

1) echo "All your base are belong to us" | tr "[a-z]" '[A-Z]"

2) echo "monkeys love bananas" | sed 's/monkey/gorilla/g'

3) echo "          monkeys love bananas" | sed 's/^[ \t]*//'

4) cat /etc/passwd | grep ^"root" | cut -d ':' -f 7

5) cat /etc/passwd | sed 's=/bin/bash=/user/bin/python=' | sed 
's=/bin/csh=/usr/bin/python=' | sed 's=/bin/tchs=/usr/bin/python=' | 
grep python 

6) cat /etc/passwd | grep -h '^4' | grep -h '7$'

7) comm -12 file1.txt file2.txt

8) comm -3 file1.txt file2.txt
