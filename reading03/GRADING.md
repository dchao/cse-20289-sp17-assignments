reading03 - Grading
===================

**Score**: 3.5/4

Deductions
----------
-0.25 - 6. That command will only return lines that begin with a 4 and end with
a 7, not lines that contain a number that starts with a 4 and ends with a 7.
-0.25 - 8. You should use the diff command.

Comments
--------
For 5, you can condense that command into this:
cat /etc/passwd | sed 's=/bin/[batc]sh=/usr/bin/python' | grep python
