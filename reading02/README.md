
Reading 02
==========
Original exists.sh
1) Using the command: /bin/bash exists.sh
2) Using the command: chmod 755 exists.sh
3) Using the command: ./exists.sh
4) It signals to the interpreter to use the Bourne Shell when 
   interpreting the following lines and to treat the following lines as 
   a script
5) README.md exists!
6) It is the variable 1 being referenced (the $ indicates it is being 
   referenced)
7) Sees if the variable 1 exists as a file
8) It outputs the first file that exists in the current directory

New exists.sh has been committed
