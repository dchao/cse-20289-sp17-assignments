#!/bin/sh

if [ $# -eq 0 ] ; then
	echo "Error: no input given"
	exit 1;
else
	for i in $@
	do
		if [ -e "$i" ] ; then
			echo "$i exists"
		else
			echo "$i does not exist"
			exit 1
		fi
	done
fi
