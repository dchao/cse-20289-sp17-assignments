Comments:

- `$1` is the first positional argument passed to the script. This script checks if the first positional argument exists as a file and prints a message. -0.25

Score: 3.75/4
