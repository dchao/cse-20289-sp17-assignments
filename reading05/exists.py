#!/usr/bin/env python2.7

import os
import sys
import os.path

if len(sys.argv) <= 1:
	print('Error')
	exit(1)
else:
	for arg in sys.argv[1:]:
		if os.path.exists(arg):
			print('%s exists!' % arg)
		elif not os.path.exists(arg):
			print('%s does not exists!' % arg)
			exit(1)
	
