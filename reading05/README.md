Reading 05
==========
Activity 1: echo.py

1) The purpose of import sys is to allow us to use functions and objects 
defined in the sys module (very similar to #include statements in C++) 

2) for arg in sys.argv[1:]: will execute whatever is inside the for loop 
(in this case, print arg,) for every argument passed atrting with the 
second argument (as objects are zero-indexed in python).  Essentially, 
for every argument passed along with echo.py, it will echo the argument.

3) The purpose of the trailing common is to have all the output be on 
the same line as opposed to each be outputted as independant lines

Activity 2: cat.py

1) args is the container for the arguements passed tothe script. For 
each iteration of the loop the first element is popped into arg, causing 
the script to march through the arguments one by one. 
len(args) > 1 makes sure that the loop will keep running until no args 
are left to be passed.  The args[0].startswith('-') checks that each 
argument passed is a flag.  Finally len(args) avoids seg faults.

2) The first if statement checks to see if no arguements were passed 
when the script is called. It appends a '-' to args if this is the case 
so that the main execution of the program knows no arguments were 
passed. Then the main execution checks to see if args = '-' in which 
case it reads from stdin

3) line = line.rstrip() removes the chars or the whitespace from the end 
of the script. It is necessary so that the script will not  add extra 
end lines to the output and subsequently not match the original file.
