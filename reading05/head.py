#!/usr/bin/env python2.7

import os
import sys

def usage(status=0):
	print '''Usage: head.py files...
	
	-n NUM		print the first NUM lines isntead of standard first 
	10'''.format(os.path.basename(sys.argv[0]))
	sys.exit(status)
	

# Declare the global variables

totOut = 10

# Parse the command line arguments
args = sys.argv[1:]
while len(args) and args[0].startswith('-') and len(args[0]) > 1:
	arg = args.pop(0)
	if arg == '-h':
		usage(0)
	elif arg == '-n':
		totOut = args[1]
	else:
		usage(1)
	
#used to check if using pipeline
if len(args) == 0:
	args.append('-')	
	
#main implementation
for path in args:
	if path == '-':
		stream = sys.stdin
	else:
		stream = open(path)
		
	counter = 0
	for line in stream:
		if counter < totOut:
			print line,
		counter = counter + 1

	stream.close()
