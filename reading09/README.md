Reading 09
==========
1) a) The size of s is 16 bytes, the size of u is 8 bytes (because of padding).
   b) The difference between a union and a struct is that unions write to the sam      same memory space for the variable whereas structs do not, therefore unions      only allocate space for whichever of its contents takes the most space

2) a) the message that the program outputs is: fate is inexorible!
   b) with the addition of the line the program sneds out 8 bytes of binary, repr      esenting the long type of v0.  This follows, seeing as v0 is the union of 8      bytes allocated.  The putc command yields the actual binary of the data typ      e and sends it out to the console, adding a space for readability.
   c) This program exhibits how every program is ultimately represented in binary      The way this data is interpreted and subsequently read changes on the data       type represented (ex. a decimal as 4 bytes of IEEE floating point notation,      a char as 8 bits of ASCII code).  When we cast data types, we are really si      mply just increasing or decresing the memory allocated for the given elemen      t

3) a) A collision is when two inputs into the hashing function generate the same       output 
   b) collisions are handled using multiplicative hasing to generate the hashes
   c) the actual inputs are stored in the array (which must be of size greater th      an or eqaul to the amount of values), thuse avoiding collisions because of       using the actual inputs as opposed to the hashes

4) a) bucket			value
	0			NULL
	1			NULL
	2			2, 72
	3			3, 13
	4			14
	5			NULL		
	6			56
	7			7,
	8			78, 68
	9			79	 
   b) bucket			value
	0			7
	1			3
	2			2
	3			78->8
	4			56->6
	5			72->2
	6			79->9
	7			68->8
	8			13->3
	9	 		14->4
