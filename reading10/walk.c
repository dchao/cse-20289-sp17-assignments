#include <stdlib.h>
#include <unistd.h>
#include <stdio.h>
#include <sys/stat.h>
#include <sys/types.h>
#include <dirent.h>

int main()
{
    DIR *dir = opendir(".");
    if (dir == NULL)
    {
        perror("unable to open directory\n");
        exit(0);
    }
    else
    {
        struct dirent *entry;
        while ((entry = readdir(dir)) != NULL)
        {
            FILE *file = fopen(entry->d_name, "r");
            int descriptor = fileno(file);
            struct stat buff;
            fstat(descriptor, &buff);
            printf("%s  %d\n", entry->d_name, (int) buff.st_size);
        }
    }
}

/* vim: set sts=4 sw=4 ts=8 expandtab ft=c: */
