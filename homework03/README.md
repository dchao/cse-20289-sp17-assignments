Homework 03
===========

Activity 1
1) The command line arguments were parsed with a case statement 
utilizing $1

2) The source sets (upper and lower case) were initialized as arrays 
where the data was the alphabet

3) The target set was constructed using the variable rot which is the 
number of rotations for the cipher, subsequently the new set, which is 
used in the final tr command, is created starting with that letter

4) 4 variables are constructed, each being the rotation number for a 
lower case and upper case and the rotation number-1 for lower case and 
upper case.  From there, tr is used to transform the source set into the 
target set as such

tr "a-z" "$lowFRot-za-$lowSRot" | tr "A-Z" "$uppFRot-ZA-$uppSRot"
where the target sets are created in the tr command call


Activity 2

1) Instead of how I parsed the commands in the first activity I parsed 
the commands in a while loop and shifted until $# = 0, looping through 
all the arguments given.  A case statement like in the first activity 
was used to identify which flags were called

2) Initially I check to see if a delimiter is given by shifting the 
argument after a -d flag, using sed to delete any lines beginning with 
the delimiter and any empty lines

3) Using sed again I delete empty lines, but not when the -W flag is 
passed

4) The command options were all done inside a function called catchFlags 
that creates an additional command in the pipeline based on the flag 
passed in the original call to the script


Activity 3

1) This time the command line arguments were parsed similarly to the 
frist activity, but it was done within a while loop to account for all 
flags passed if multiple were passed, using $# and shift to 
appropriately use the flags and the specifications passed

2) The zip codes are extracted using the curl command to pull the data 
from the website based on the specific state if a different state 
argument was passed to the program

3) State was filtered differently than city since state was used 
initially in the curl command so no extra filtering was necessary.  City 
was filtered by using grep before piping it into another grep to get the 
zipcodes

4) The text and csv options were handled by having text be the default 
and then using sed to transform the formatting into csv format if the 
user passed the -f flag
