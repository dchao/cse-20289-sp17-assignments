#!/bin/bash

usage()
{
	cat<<EOF
	$ ./broify.sh -h
	Usage: broify.sh

	 -d DELIM    Use this as the comment delimiter.
	 -W          Don't strip empty lines.
EOF
	exit 1
}
catchFlags()
{
	if [ $# -eg 0 ]; then
		sed '/^ *$/d'
	else
		while [ $# -gt 0 ]
		do
			case $1 in
				-h)
					usage
				;;
				-d)
					if [ $2 != ""]; then
						delim = $2
					else
						delim = '#'
					fi

					sed "/${delim}.*/d" | sed'/^ *$/d'
				;;
			esac
		shift
		done
	fi
}

echo $text | catchFlags$@ | sed -Ee 's|[[:space:]]*$||'


