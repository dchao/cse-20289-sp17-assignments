Homework 01 - Grading
====================

**Score**: 12/15

Deductions
----------

broify
-.25 doesnt handle the -W flag
-.25 doesnt remove empty lines
-.25 doesnt remove empty lines

-1 tests mostly fail

zipcode
-.25 doesnt properly filter city zip codes if -c is specified
-1 tests mostly fail

Comments
--------
- in your broify script you echo "$text" but never set the text variable so the program does not run correctly
