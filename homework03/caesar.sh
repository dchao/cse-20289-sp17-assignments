#!/bin/bash

usage()
{
	cat<<EOF
	Usage: caesar.sh [rotation]

	This program will read from stdin and rotate (shift right) the text 
	by
	the specified rotation.  If none is specified, then the default 
	value is
	13.
EOF
exit 1
}

case $1 in
	"-h")
	usage
	;;
	*)
	if [ $# -eq 0 ] ; then	#default setting of ROT13
		rot=13
	else
		if [ $1 -gt 26 ]; then
			rot=$1%26
		else
			rot=$1
		fi
	fi
	;;
esac

#arrays of both lower and uppercase letters used to help rotate the cypher
Lletters=({a..z})
Uletters=({A..Z})
#used to help find the second point of rotation for the second set
help=1
sRot=`expr $rot - $help`

# setting rotation variables to what the rotation cypher number is set
# to by using the letter at that index in the array
lowFRot=${Lletters[rot]}
lowSRot=${Lletters[sRot]}
uppFRot=${Uletters[rot]}
uppSRot=${Uletters[sRot]}

#replacing whats passed with the rotation cypher
tr 'a-z' "$lowFRot-za-$lowSRot" | tr 'A-Z' "$uppFRot-ZA-$uppSRot"
