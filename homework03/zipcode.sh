#!/bin/bash

usage()
{
	cat<<EOF
	Usage: zipcode.sh

	  -c      CITY    Which city to search
	  -f      FORMAT  Which format (text, csv)
	  -s      STATE   Which state to search (Indiana)
	  If not CITY is specified, then all the zip codes for the STATE 
	  are displayed.
EOF

exit 1
}

#Setting defaults for the variables if they are not specified by the user
format=text
city=none
state=Indiana

#iterate through all possible flags passed
while [ $# -gt 0 ]
do
	case $1 in
		-h)
			usage
			;;
		-f)
			format=$2
			shift
			;;
		-c)
			city=$2
			shift
			;;
		-s)
			state=$2
			shift
			;;
	esac
	#need to shift again in the scenario of multiple flags being passed
	shift
done

# When using grep here, iterating through all the data and then using
# the -o flag to only output what grep matched, in this case the
# zipcodes because of the [0-9]{5} specification
if [ "$city" == none ] && [ $format == text ]; then
	curl -sL http://www.zipcodestogo.com/$state/ | grep -Eo [0-9]{5}
elif [ "$city" == none ] && [ $format == csv ]; then
	curl -sL http://www.zipcodestogo.com/$state/ | grep -Eo '>[0-9]{5}<' | grep -Eo [0-9]{5} | tr '\n' ',' | sed 's/.$/\n/'
elif [ "$city" != none ] && [ $format == text ]; then
	curl -sL http://www.zipcodestogo.com//$state/ | grep -E "/$city/" | grep -Eo '>[0-9]{5}<' | grep -Eo [0-9]{5}
elif [ "$city" != none ] && [ $format == csv ]; then
	curl -sL http://www.zipcodestogo.com//$state/ | grep -E "/$city/" | grep -Eo '>[0-9]{5}<' | grep -Eo [0-9]{5} | tr '\n' ',' | sed 's/.$//'
fi
